||| [dotgen][1] porting in Idris 2.
|||
||| The original dotgen license is below:
|||
||| > Copyright (c) 2008 Andy Gill
||| > All rights reserved.
||| >
||| > Redistribution and use in source and binary forms, with or without
||| > modification, are permitted provided that the following conditions
||| > are met:
||| > 1. Redistributions of source code must retain the above copyright
||| >    notice, this list of conditions and the following disclaimer.
||| > 2. Redistributions in binary form must reproduce the above copyright
||| >    notice, this list of conditions and the following disclaimer in the
||| >    documentation and/or other materials provided with the distribution.
||| > 3. The names of the authors may not be used to endorse or promote products
||| >    derived from this software without specific prior written permission.
||| >
||| > THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
||| > IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
||| > OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
||| > IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
||| > INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
||| > NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
||| > DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
||| > THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
||| > (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
||| > THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|||
||| Some implementations are cited from Idris 2 `Prelude.Show` private functions.
||| Idris 2 license is below:
|||
||| > Copyright (c) 2020 Edwin Brady
||| >     School of Computer Science, University of St Andrews
||| > All rights reserved.
||| >
||| > This code is derived from software written by Edwin Brady
||| > (ecb10@st-andrews.ac.uk).
||| >
||| > Redistribution and use in source and binary forms, with or without
||| > modification, are permitted provided that the following conditions
||| > are met:
||| > 1. Redistributions of source code must retain the above copyright
||| >    notice, this list of conditions and the following disclaimer.
||| > 2. Redistributions in binary form must reproduce the above copyright
||| >    notice, this list of conditions and the following disclaimer in the
||| >    documentation and/or other materials provided with the distribution.
||| > 3. None of the names of the copyright holders may be used to endorse
||| >    or promote products derived from this software without specific
||| >    prior written permission.
||| >
||| > THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
||| > EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
||| > IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
||| > PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE
||| > LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
||| > CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
||| > SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
||| > BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
||| > WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
||| > OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
||| > IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
||| >
||| > *** End of disclaimer. ***
|||
||| [1]: https://hackage.haskell.org/package/dotgen
module Text.Dot

import Data.String
import Data.String.Extra
import Data.SortedMap
import Data.SortedSet

%default total

infix 1 .->.

-- Do not public export, because `MkNodeId` has an id of its rule.
export
data NodeId : Type where
  MkNodeId     : (id : String) -> NodeId
  MkUserNodeId : (id : Int)    -> NodeId

%name NodeId id

export
implementation Show NodeId where
  show (MkNodeId str)   = str
  show (MkUserNodeId i) = if i < 0
    then "u_" ++ show (negate i)
    else "u"  ++ show i

-- TODO: Need to export this, what interface should be?
Attributes : Type
Attributes = SortedMap String String

data GraphElement : Type where
  GraphAttribute : (key : String) -> (value : String)                 -> GraphElement
  GraphNode      : NodeId -> Attributes                               -> GraphElement
  GraphEdge      : (srcid : NodeId) -> (dstid : NodeId) -> Attributes -> GraphElement
  GraphEdge'     :  (srcid : NodeId) -> (srclabel : (Maybe String))
                 -> (dstid : NodeId) -> (dstlabel : (Maybe String))
                 -> Attributes                                        -> GraphElement
  Scope          : (List GraphElement)                                -> GraphElement
  SubGraph       : NodeId -> (List GraphElement)                      -> GraphElement

%name GraphElement elem

export
record Dot a where
  constructor MkDot
  unDot : Int -> (List GraphElement, Int, a)

%name Dot dot

export
implementation Functor Dot where
  map f (MkDot unDot) = MkDot $ \i =>
    let (es, j, a) := unDot i in (es, j, f a)

export
implementation Applicative Dot where
  pure a = MkDot ([],, a)

  (MkDot f) <*> (MkDot x) = MkDot $ \i =>
    let (es, j, g) := f i
        (fs, k, y) := x j
     in (es <+> fs, k, g y)

export
implementation Monad Dot where
  (MkDot x) >>= f = MkDot $ \i =>
    let (es, i', x')    := x i
        (es', i'', x'') := unDot (f x') i'
     in (es <+> es', i'', x'')

||| Takes a list of attributes, generates a new node, and gives a `NodeId`.
export
node : Attributes -> Dot NodeId
node attrs = MkDot $ \i =>
  let nid := MkNodeId $ 'n' <+ show i
   in ([GraphNode nid attrs], i + 1, nid)

||| Allows a user to use their own node id's, without needing to remap them.
export
userNodeId : Int -> NodeId
userNodeId = MkUserNodeId

||| Takes a `NodeId`, and adds some attributes to that node.
export
userNode : NodeId -> Attributes -> Dot ()
userNode nid attrs = MkDot ([GraphNode nid attrs],, ())

||| Generates an edge between two `NodeId`s, with attributes.
export
edge : NodeId -> NodeId -> Attributes -> Dot ()
edge from to attrs = MkDot ([GraphEdge from to attrs],, ())

||| Generates an edge between two `NodeId`s, with optional node sub-labels, and attributes.
export
edge' : NodeId -> Maybe String
      -> NodeId -> Maybe String
      -> Attributes
      -> Dot ()
edge' from optF to optT attrs = MkDot ([GraphEdge' from optF to optT attrs],, ())

||| Generates an edge between two `NodeId`s.
export
(.->.) : NodeId -> NodeId -> Dot ()
(.->.) from to = edge from to empty

||| Groups a subgraph together; in dot these are the subgraphs inside "{" and "}".
export
scope : Dot a -> Dot a
scope (MkDot fn) = MkDot $ \i =>
  let (elems, i', a) := fn i
   in ([Scope elems], i', a)

||| When a set of nodes share specific attributes. Usually used for layout tweaking.
export
share : Attributes -> List NodeId -> Dot ()
share attrs nodeids = MkDot $ \i =>
  ( [ Scope $
        (map (\(name, val) => GraphAttribute name val) (SortedMap.toList attrs))
          ++ [GraphNode nodeid empty | nodeid <- nodeids]
    ]
  , i
  , ()
  )

||| Provides a combinator for a common pattern; a set of `NodeId`s with the same rank.
export
same : List NodeId -> Dot ()
same = share $ fromList [("rank", "same")]

||| Builds an explicit, internally named subgraph (called cluster).
export
cluster : Dot a -> Dot (NodeId, a)
cluster (MkDot fn) = MkDot $ \i =>
  let cid             := MkNodeId $ "cluster_" ++ show i
      (elems, uq', a) := fn (i + 1)
   in ([SubGraph cid elems], uq', (cid, a))

||| Gives a attribute to the current scope.
export
attribute : (String, String) -> Dot ()
attribute (name, val) = MkDot ([GraphAttribute name val],, ())

-- From Prelude.Show
firstCharIs : (Char -> Bool) -> String -> Bool
firstCharIs p ""  = False
firstCharIs p str = p (assert_total (prim__strHead str))

-- From Prelude.Show
protectEsc : (Char -> Bool) -> String -> String -> String
protectEsc p f s = f ++ (if firstCharIs p s then "\\&" else "") ++ s

-- From Prelude.Show
showLitChar : Char -> String -> String
showLitChar '\a'   = ("\\a" ++)
showLitChar '\b'   = ("\\b" ++)
showLitChar '\f'   = ("\\f" ++)
showLitChar '\n'   = ("\\n" ++)
showLitChar '\r'   = ("\\r" ++)
showLitChar '\t'   = ("\\t" ++)
showLitChar '\v'   = ("\\v" ++)
showLitChar '\SO'  = protectEsc (== 'H') "\\SO"
showLitChar '\DEL' = ("\\DEL" ++)
showLitChar '\\'   = ("\\\\" ++)
showLitChar c
    = case getAt (fromInteger (prim__cast_CharInteger c)) asciiTab of
           Just k => strCons '\\' . (k ++)
           Nothing => if (c > '\DEL')
                         then strCons '\\' . protectEsc isDigit (show (prim__cast_CharInt c))
                         else strCons c
  where
    asciiTab : List String
    asciiTab
        = ["NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL",
           "BS",  "HT",  "LF",  "VT",  "FF",  "CR",  "SO",  "SI",
           "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
           "CAN", "EM",  "SUB", "ESC", "FS",  "GS",  "RS",  "US"]

showsDotChar : Char -> (List Char -> List Char)
showsDotChar '"' = (\s => unpack $ "\\\"" ++ pack s)
showsDotChar x   = (\s => unpack $ showLitChar x (pack s))

showAttr : (String, String) -> String
showAttr (name, val) = name ++ "=\"" ++ (pack $ foldr showsDotChar [] (unpack val)) ++ "\""

showAttrs : Attributes -> String
showAttrs = showAttrs'' . toList
  where
    showAttrs'' : List (String, String) -> String
    showAttrs'' [] = ""
    showAttrs'' (x :: xs) = "[" ++ showAttrs' x xs ++ "]"
      where showAttrs' : (String, String) -> List (String, String) -> String
            showAttrs' x [] = showAttr x
            showAttrs' x (y :: ys) = showAttr x ++ "," ++ showAttrs' y ys

showGraphElement : GraphElement -> String
showGraphElement (GraphAttribute name val)            = showAttr (name, val) ++ ";"
showGraphElement (GraphNode nid attrs)                =
  show nid ++ showAttrs attrs ++ ";"
showGraphElement (GraphEdge from to attrs)            =
  show from ++ " -> " ++ show to ++ showAttrs attrs ++ ";"
showGraphElement (GraphEdge' from optF to optT attrs) =
  showName from optF ++ " -> " ++ showName to optT ++ showAttrs attrs ++ ";"
    where
      showName : NodeId -> Maybe String -> String
      showName n Nothing = show n
      showName n (Just t) = show n ++ ":" ++ t
showGraphElement (Scope elems)                        = assert_total $
  "{\n" ++ unlines (map showGraphElement elems) ++ "\n}"
showGraphElement (SubGraph nid elems)                 = assert_total $
  "subgraph " ++ show nid ++ " {\n" ++ unlines (map showGraphElement elems) ++ "\n}"

||| Renders a dot graph as a `String`.
export
showDot : Dot a -> String
showDot (MkDot dm) =
  let (elems, _, _) := dm 0
   in "digraph G {\n" ++ unlines (map showGraphElement elems) ++ "\n}\n"

netlistGraphHelper : (fm : SortedMap a NodeId) -> (src : a) -> (dst : a) -> Dot ()
netlistGraphHelper fm src dst = do
  let Just src' := SortedMap.lookup src fm | _ => pure ()
      Just dst' := SortedMap.lookup dst fm | _ => pure ()
  src' .->. dst'

||| Generates a simple graph from a netlist.
||| @ attrFn Attributes for each node
||| @ outFn Out edges leaving each node
||| @ assocs The netlist
export
netlistGraph :
  Ord a =>
  (b -> List (String, String)) ->
  (b -> List a) ->
  List (a, b) ->
  Dot ()
netlistGraph attrFn outFn assocs = do
  let nodes := SortedSet.fromList $ [ a | (a, _) <- assocs ]
  let outs := SortedSet.fromList $ [ o | (_, b) <- assocs, o <- outFn b ]
  nodeTab <- sequence [ do nd <- node $ fromList (attrFn b)
                           pure (a, nd)
                      | (a, b) <- assocs ]
  otherTab <- sequence [ do nd <- node $ fromList []
                            pure (o, nd)
                       | o <- SortedSet.toList outs
                       , not $ o `contains` nodes ]
  let fm = SortedMap.fromList $ nodeTab ++ otherTab
  sequence_ [ netlistGraphHelper fm dst src
            | (dst, b) <- assocs
            , src <- outFn b ]
  pure ()
