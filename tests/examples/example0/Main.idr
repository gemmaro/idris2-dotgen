import Text.Dot

main : IO ()
main = putStrLn $ showDot $ do
  attribute ("rankdir", "LR")
  o <- node $ fromList [("label", "one")]
  t <- node $ fromList [("label", "two")]
  o .->. t
