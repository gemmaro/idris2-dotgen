import Test.Golden

%default total

examplesTests : TestPool
examplesTests = MkTestPool "example tests" [] Default
  [ "example0" ]

covering
main : IO ()
main = runner
  [ testPaths "examples" examplesTests ]
  where
    testPaths : (dir : String) -> TestPool -> TestPool
    testPaths dir = { testCases $= map ((dir ++ "/") ++) }
